<?php

namespace Larafast\Forms\src;

use Kris\LaravelFormBuilder\Form;

abstract class FormAbstract extends Form
{
    public function __construct()
    {
        $this->setMethod('POST');
        $this->setFormOption('id', 'form_' . md5(get_class($this)));
    }
}
