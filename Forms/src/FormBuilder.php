<?php

namespace Larafast\Forms\src;

use Kris\LaravelFormBuilder\FormBuilder as BaseFormBuilder;

class FormBuilder extends BaseFormBuilder
{
    public function create($formClass, array $options = [], array $data = [])
    {
        return parent::create($formClass, $options, $data);
    }
}