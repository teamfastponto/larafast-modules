<?php namespace Larafast\Forms\Builders;

use Larafast\Forms\src\FormAbstract;

class LoginForm extends FormAbstract
{
    public function buildForm()
    {
        $this
            ->add('email', 'email', [
                'label' => 'E-mail'
            ])
            ->add('password', 'password', [
                'label' => 'Senha'
            ])
            ->add('submit', 'submit', [
                'label' => 'Logar <i class="fa fa-user"></i>'
            ]);
    }
}