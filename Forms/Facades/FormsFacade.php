<?php

namespace Larafast\Forms\Facades;

use Illuminate\Support\Facades\Facade;
use Larafast\Forms\src\FormBuilder;

class FormsFacade extends Facade{

    protected static function getFacadeAccessor()
    {
        return FormBuilder::class;
    }
}