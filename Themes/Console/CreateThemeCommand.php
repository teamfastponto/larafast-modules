<?php

namespace Larafast\Themes\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Support\Str;

class CreateThemeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larafast:make-theme';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cria um ThemeSchema para estruturação de elementos e dados';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $themeName = $this->setThemeName();

        $this->createDirectoryTheme($themeName);
        $this->createDirectoryThemeAsset($themeName);

        Artisan::call('larafast:make-theme-schema ' . $themeName);

        $this->info('Tema criado com sucesso');
    }

    function createDirectoryTheme($themeName){
        $foldersThemeCreate = [
            'elements',
            'layouts',
            'views'
        ];

        if(File::exists(base_path('themes/' . $themeName))){
            $this->info($themeName . ' Já existe');
        }

        foreach($foldersThemeCreate as $folder){
            $folderCreate = base_path('themes/' . $themeName . '/'. $folder);
                File::makeDirectory($folderCreate, 0755, true);
                $this->info($folderCreate . ' Criado com sucesso!');
        }
    }

    function createDirectoryThemeAsset($themeName){
        $foldersThemeAssetCreate = [
            'js',
            'css',
            'fonts',
            'img',
            'vendor'
        ];

        if(File::exists(public_path('themes/' . $themeName))){
            $this->info($themeName . ' Já existe');
        }

        foreach($foldersThemeAssetCreate as $folder){
            $folderCreate = public_path('themes/' . $themeName . '/'. $folder);
                File::makeDirectory($folderCreate, 0755, true);
                $this->info($folderCreate . ' Criado com sucesso!');
        }
    }

    function setThemeName(){
        $themeName = $this->ask('Qual é o nome do tema? Snake case (theme_exemple)');
        $themeName = Str::of($themeName)->snake();

        if ($this->confirm('Confirma nome do tema? ' . $themeName, true)) {
            return $themeName;
        }else{
           $this->setThemeName(); 
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['example', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
