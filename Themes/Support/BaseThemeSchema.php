<?php

namespace Larafast\Themes\Support;

use Illuminate\Support\Facades\View;

abstract class BaseThemeSchema{

    protected static $name;
    protected static $prefix;
    protected static $elements;
    
    public static function getPrefix(){
        if(is_null(static::$prefix) || empty(static::$prefix)){
            return static::$name;
        }else{
            return static::$prefix;
        }
    }

    public static function getElements(){
        $elements = [];
        $keysElement = ['name', 'view'];
        $pathView = 'themes::' . static::$name . '.elements.';

        foreach(static::$elements as $element){
            if (count($keysElement) === count(array_filter(array_keys($element), function($key) use ($keysElement) { return in_array($key, $keysElement); })))
            {
                if(!View::exists($pathView . $element['view'])){
                    continue;
                }

                $element['view'] = $pathView . $element['view'];

                $element = array_filter($element, function($v) use ($keysElement) {
                    return in_array($v, $keysElement);
                }, ARRAY_FILTER_USE_KEY);

                array_push($elements, $element);

            } else {
                continue;
            }
        }

        return $elements;
    }

    public static function getTypeDataElement($element, $ref){
        $elements = static::$elements;
        $element = collect($elements)->where('name', $element)->first();
        $item = collect($element['items'])->where('name', $ref)->first();

        return $item;
    }
}