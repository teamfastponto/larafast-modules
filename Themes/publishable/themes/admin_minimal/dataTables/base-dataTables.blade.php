<div class="table-responsive">
    <table
    class="table align-items-center table-striped"
    id="{{ $idTable }}"
    data-model="{{ encrypt($model) }}"
    data-joins="{{ encrypt(json_encode($joins)) }}"
    data-fields="{{ json_encode($fields, JSON_UNESCAPED_UNICODE) }}"
    data-menu="{{ json_encode($menu, JSON_UNESCAPED_UNICODE) }}"
    data-route="{{ $route }}"
    >
      <thead class="thead-light">
        <tr>
            @foreach ($fields as $field)
                <th>{{ $field['friendly_name'] }}</th>
            @endforeach
        </tr>
      </thead>
      <tfoot>
        <tr>
            @foreach ($fields as $field)
                <th>{{ $field['friendly_name'] }}</th>
            @endforeach
        </tr>
      </tfoot>
      <tbody>
      </tbody>
    </table>
</div>

<link href="{{ theme_asset('admin_minimal') }}/vendor/datatables/datatables.min.css" rel="stylesheet">
<script src="{{ theme_asset('admin_minimal') }}/js/jquery-3.3.1.min.js"></script>
<script src="{{ theme_asset('admin_minimal') }}/vendor/datatables/datatables.min.js"></script>

<script>
    const $table = $('#{{ $idTable }}');
    const model = $table.data('model');
    const joins = $table.data('joins');
    const fields = $table.data('fields');
    const menu = $table.data('menu');
    const route = $table.data('route');

    console.log(fields);

    $($table).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: route,
            type: "POST",
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: function(d){
                d.model = model;
                d.joins = joins;
                d.menu = menu;

                return d;
            }
        },
        columns: fields
    });
</script>