<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h2 class="h3 mb-0 text-gray-800">{{ Arr::get($props, 'title_header', 'title') }}</h2>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            @foreach (Arr::get($props ?? null, 'breadcrumb', []) as $breadcrumbItem)
                @if ($loop->last)
                    <li class="breadcrumb-item active" aria-current="page">
                        {{ $breadcrumbItem['name'] }}
                    </li>
                @else
                    <li class="breadcrumb-item" aria-current="page">
                        <a href="{{ $breadcrumbItem['url'] }}">{{ $breadcrumbItem['name'] }}</a>
                    </li>
                @endif
            @endforeach
        </ol>
    </nav>
</div>
