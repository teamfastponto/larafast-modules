<div class="col-12 p-0">
    <div class="card">
        <div class="card-header">
            <h4>{{ Arr::get($props ?? null, 'titleCard', 'props: titleCard') }}</h4>
        </div>
        <div class="card-body">
            {{ $contentCard ?? 'slot: contentCard' }}
        </div>
    </div>
</div>
