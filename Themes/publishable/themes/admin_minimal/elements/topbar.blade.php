<div class="navbar-bg"></div>
<nav class="navbar navbar-expand-lg main-navbar sticky">
    <div class="form-inline mr-auto">
        <ul class="navbar-nav mr-3">
            <li>
                <a href="#" data-toggle="sidebar" class="nav-link nav-link-lg collapse-btn">
                    <i data-feather="align-justify"></i>
                </a>
            </li>
            <li>
                <a href="#" class="nav-link nav-link-lg fullscreen-btn">
                    <i data-feather="maximize"></i>
                </a>
            </li>
            @if(config('admin.topbar.search'))
                <li>
                    <form class="form-inline mr-auto">
                        <div class="search-element">
                        <input class="form-control" type="search" placeholder="{{ config('admin.topbar.label_search') }}" aria-label="Search" data-width="200">
                        <button class="btn" type="submit">
                            <i class="fas fa-search"></i>
                        </button>
                        </div>
                    </form>
                </li>
            @endif
        </ul>
    </div>
    @foreach (topMenu() as $topMenu)
        <li class="dropdown dropdown-list-toggle">
            <a href="#" data-toggle="dropdown" id="dropdown-{{ $topMenu['name'] }}" class="nav-link nav-link-lg message-toggle">
                <i data-feather="{{ $topMenu['icon'] }}"></i>
                <span class="badge headerBadge1">{{ $topMenu['counter'] }}</span>
            </a>
            <div class="dropdown-menu dropdown-list dropdown-menu-right pullDown">
                <div class="dropdown-header">
                    {{ $topMenu['friendly_name'] }}
                </div>
                @if(isset($topMenu['content']['element']) && isset($topMenu['content']['props']))
                    <x-element name="{{ $topMenu['content']['element'] }}" :props="$topMenu['content']['props']"></x-element>
                @endisset
                <div class="dropdown-footer text-center">
                    <a href="{{ $topMenu['url_show_more'] }}">{{ $topMenu['label_show_more'] }}<i class="fas fa-chevron-right"></i></a>
                </div>
            </div>
        </li>
    @endforeach

    <ul class="navbar-nav navbar-right">
        <li class="dropdown">
            <a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                <img alt="image" src="{{ config('admin.topbar.user_menu.url_image') }}" class="user-img-radious-style">
                <span class="d-sm-none d-lg-inline-block"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right pullDown">
                <div class="dropdown-title">{{ config('admin.topbar.user_menu.name') }}</div>
                    @foreach (userMenu() as $item)
                        <a class="dropdown-item has-icon" href="#">
                            <i class="{{ $item['icon'] }} fa-sm fa-fw mr-2 text-gray-400"></i>
                            {{ $item['friendly_name'] }}
                        </a>
                    @endforeach
                <div class="dropdown-divider"></div>
                <a href="javascript:void(0);" class="dropdown-item has-icon text-danger" data-toggle="modal" data-target="#logoutModal"> <i class="fas fa-sign-out-alt"></i>
                    Logout
                </a>
            </div>
        </li>
    </ul>
</nav>

<!-- Modal Logout -->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelLogout" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabelLogout">Ohh No!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to logout?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Cancel</button>
                <a href="{{ route('logout') }}" class="btn btn-primary" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
                    Logout
                </a>

                <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
</div>
