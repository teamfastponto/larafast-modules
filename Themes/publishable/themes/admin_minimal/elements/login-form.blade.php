@extends('themes::' . adminTheme() . '.layouts.master-login')
@section('login-title')
    Fazer Login
@endsection
@section('login-content')
<div class="login-form">
    <div class="text-center">
        <h1 class="h4 text-gray-900 mb-4">Logo</h1>
    </div>
    {!! form($form) !!}
    <hr>
    {{-- <div class="text-center">
        <a class="font-weight-bold small" href="register.html"></a>
    </div> --}}
</div>
@endsection
