<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="#">
                <img alt="logo" src="{{ config('admin.logo_url') }}" class="header-logo" />
                <span>{{ config('admin.name_panel') }}</span>
            </a>
        </div>

        <ul class="sidebar-menu">
            @foreach (mainMenu()->pluck('session')->unique() as $session)

                @if(!is_null($session))
                    <li class="menu-header">
                        {{ $session }}
                    </li>
                @endif

                @foreach (mainMenu()->where('session', $session) as $menu)

                    @if($menu['hasChildren'])
                        <li class="dropdown">
                            <a href="#" class="menu-toggle nav-link has-dropdown">
                                <i class="{{ $menu['icon'] }}"></i>
                                <span class="pl-2">{{ $menu['friendly_name'] }}</span>
                            </a>
                            <ul class="dropdown-menu">
                                @foreach ($menu['childrens'] as $children)
                                    <a class="nav-link" href="{{ $children['url'] }}">{{ $children['friendly_name'] }}</a>
                                @endforeach
                            </ul>
                        </li>
                    @else
                        <li class="dropdown active {{ $loop->first ? 'active' : null }}">
                            <a href="{{ $menu['url'] }}" class="nav-link">
                                <i class="{{ $menu['icon'] }}"></i>
                                <span class="pl-2">{{ $menu['friendly_name'] }}</span>
                            </a>
                        </li>
                    @endif

                @endforeach

            @endforeach
        </ul>
    </aside>
</div>
