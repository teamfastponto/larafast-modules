<script src="{{ theme_asset('admin_minimal') }}/js/jquery-3.3.1.min.js"></script>
<script src="{{ theme_asset('admin_minimal') }}/js/popper.min.js"></script>
<script src="{{ theme_asset('admin_minimal') }}/js/bootstrap.min.js"></script>
<script src="{{ theme_asset('admin_minimal') }}/vendor/jquery-selectric/jquery.selectric.min.js"></script>
<script src="{{ theme_asset('admin_minimal') }}/vendor/jquery-steps/jquery.steps.min.js"></script>
<script src="{{ theme_asset('admin_minimal') }}/vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="{{ theme_asset('admin_minimal') }}/vendor/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="{{ theme_asset('admin_minimal') }}/vendor/jqvmap/dist/jquery.vmap.min.js"></script>
<script src="{{ theme_asset('admin_minimal') }}/vendor/jquery.sparkline.min.js"></script>

<!-- JS Libraies -->
<script src="{{ theme_asset('admin_minimal') }}/vendor/apexcharts/apexcharts.min.js"></script>
<!-- Page Specific JS File -->
<script src="{{ theme_asset('admin_minimal') }}/js/index.js"></script>
<!-- <script src="{{ theme_asset('admin_minimal') }}/js/chat.js"></script> -->
<script src="{{ theme_asset('admin_minimal') }}/vendor/jquery.nicescroll-master/jquery.nicescroll.min.js"></script>
<script src="{{ theme_asset('admin_minimal') }}/vendor/cleave-js/dist/cleave.min.js"></script>
<script src="{{ theme_asset('admin_minimal') }}/vendor/cleave-js/dist/addons/cleave-phone.us.js"></script>
<script src="{{ theme_asset('admin_minimal') }}/js/form-wizard.js"></script>
<!-- <script src="{{ theme_asset('admin_minimal') }}/js/forms-advanced-forms.js"></script> -->
<script src="{{ theme_asset('admin_minimal') }}/vendor/bootstrap-daterangepicker/daterangepicker.js"></script>

<script src="{{ theme_asset('admin_minimal') }}/vendor/moment/moment.js"></script>
<script src="{{ theme_asset('admin_minimal') }}/vendor/chartjs/chart.min.js"></script>
<script src="{{ theme_asset('admin_minimal') }}/vendor/echart/echarts.js"></script>
<script src="{{ theme_asset('admin_minimal') }}/vendor/fullcalendar/fullcalendar.min.js"></script>
<script src="{{ theme_asset('admin_minimal') }}/vendor/codemirror/lib/codemirror.js"></script>
<!-- Template JS File -->
<script src="{{ theme_asset('admin_minimal') }}/js/feather.min.js"></script>
<script src="{{ theme_asset('admin_minimal') }}/js/scripts.js"></script>
<!-- Custom JS File -->
<script src="{{ theme_asset('admin_minimal') }}/js/custom.js"></script>

<script src="{{ theme_asset('admin_minimal') }}/vendor/fontawesome-5/js/all.min.js"></script>
