<!DOCTYPE html>
<html lang="en">

<head>
<title>@yield('titlePage')</title>
@include(viewTheme(adminTheme(), 'layouts.header'))
</head>

<body>
    <div class="loader"></div>
    <div id="app">
        <div class="main-wrapper main-wrapper-1">
            {{-- sidebar --}}
            <x-element name="{{ adminElement('sidebar') }}" />
            <x-element name="{{ adminElement('topbar') }}" />

            <div class="main-content">
                <section class="section">
                    <!-- Container Fluid-->
                    <div class="section-body">
                        @yield('content')
                    </div>
                    <!---Container Fluid-->
                </section>
            </div>
        </div>

    </div>

  @include(viewTheme(adminTheme(), 'layouts.scripts'))
  @stack('custom-scripts')
</body>

</html>
