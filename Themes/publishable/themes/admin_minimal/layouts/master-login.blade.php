<!DOCTYPE html>
<html lang="pt_br">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="{{ theme_asset('admin_minimal') }}/img/logo/logo.png" rel="icon">
  <title>@yield('login-title')</title>
  <link href="{{ theme_asset('admin_minimal') }}/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="{{ theme_asset('admin_minimal') }}/vendor/fontawesome-5/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="{{ theme_asset('admin_minimal') }}/css/style.css" rel="stylesheet">

</head>

<body>
    <div id="app">
        <!-- Login Content -->
        <section class="section">
            <div class="container mt-5">
                <div class="row">
                    <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h4>Login</h4>
                            </div>
                            <div class="card-body">
                                @yield('login-content')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
  <!-- Login Content -->
  <script src="{{ theme_asset('admin_minimal') }}/js/jquery-3.3.1.min.js"></script>
  <script src="{{ theme_asset('admin_minimal') }}/js/popper.min.js"></script>
  <script src="{{ theme_asset('admin_minimal') }}/js/bootstrap.min.js"></script><script src="{{ theme_asset('admin_minimal') }}/js/feather.min.js"></script>
  <script src="{{ theme_asset('admin_minimal') }}/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="{{ theme_asset('admin_minimal') }}/js/custom.js"></script>
  <script src="{{ theme_asset('admin_minimal') }}/vendor/fontawesome-5/js/all.min.js"></script>
</body>

</html>
