<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<link href="img/logo/logo.png" rel="icon">
<link href="{{ theme_asset('admin_minimal') }}/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="{{ theme_asset('admin_minimal') }}/vendor/fontawesome-5/css/all.min.css" rel="stylesheet" type="text/css">

<link href="{{ theme_asset('admin_minimal') }}/vendor/jquery-selectric/selectric.css" rel="stylesheet">
<link href="{{ theme_asset('admin_minimal') }}/vendor/jqvmap/dist/jqvmap.min.css" rel="stylesheet">
<!-- Template CSS -->
<link href="{{ theme_asset('admin_minimal') }}/css/style.css" rel="stylesheet">
<link href="{{ theme_asset('admin_minimal') }}/css/components.css" >
<!-- Custom style CSS -->
<link href="{{ theme_asset('admin_minimal') }}/css/custom.css" rel="stylesheet">
<link rel='shortcut icon' type='image/x-icon' href='{{ theme_asset('admin_minimal') }}/img/favicon.ico' />
