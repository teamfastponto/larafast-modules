@extends('themes::' . adminTheme() . '.layouts.master')
@section('titlePage')
Hello World
@endsection
@section('content')
    <x-element :name="adminElement('header-page')" :props="[
        'title_header' => 'Hello World',
        'breadcrumb' => [
            [
                'name' => 'Hello',
                'url' => '#'
            ],
            [
                'name' => 'World',
                'url' => '#'
            ]
        ]
    ]"/>

    <x-element :name="adminElement('card-full')">
        {{-- <x-slot name="contentCard">
            Content
        </x-slot> --}}
    </x-element>
@endsection
