<?php

namespace Larafast\Themes\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Log;
use Larafast\Themes\View\Components\ElementComponent;

class ThemesServiceProvider extends ServiceProvider
{
    /**
     * @var string $moduleName
     */
    protected $moduleName = 'Themes';

    /**
     * @var string $moduleNameLower
     */
    protected $moduleNameLower = 'themes';

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerCommands();
        $this->registerViews();
        $this->registerComponents();
        $this->loadMigrationsFrom(dirname(__DIR__, 1) . '/Database/Migrations');

        $this->publishThemes();
    }

    public function publishThemes(){
        $this->publishes([
            dirname(__DIR__, 1) . '/Config/themes.php' => config_path('themes.php'),
        ], ['themes', 'config-themes']);

        $this->publishes([
            dirname(__DIR__, 1) . '/publishable/themes' => base_path('themes'),
        ], ['themes', 'view-themes']);

        $this->publishes([
            dirname(__DIR__, 1) . '/publishable/assets' => public_path('themes'),
        ], ['themes', 'asset-themes']);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->mergeConfigFrom(
            dirname(__DIR__, 1) . '/Config/themes.php', $this->moduleNameLower
        );
    }

    protected function registerCommands()
    {
        $this->commands([
            \Larafast\Themes\Console\CreateThemeCommand::class,
            \Larafast\Themes\Console\CreateThemeSchemaCommand::class
        ]);
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $this->loadViewsFrom(base_path('themes'), $this->moduleNameLower);
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/' . $this->moduleNameLower);

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
        } else {
            $this->loadTranslationsFrom(dirname(__DIR__, 1) . '/Resources/lang', $this->moduleNameLower);
        }
    }

    public function registerComponents(){
        Blade::component('element', ElementComponent::class);

        foreach(config('themes.themes') as $theme => $themeConfig){
            if(!isset($themeConfig['themeSchema']) || !class_exists($themeConfig['themeSchema'] ?? null)){
                continue;
            }

            $elements =  $themeConfig['themeSchema']::getElements();
            $prefix = $themeConfig['themeSchema']::getPrefix();

            foreach($elements as $element){
                $this->configureComponent($prefix, $element);
            }
        }
    }

    public function configureComponent($prefix, $element){
        if(isset($element['view']) && isset($element['name'])){
            Blade::component($element['view'], $prefix . '-' . $element['name']);
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
