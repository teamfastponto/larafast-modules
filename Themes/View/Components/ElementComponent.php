<?php

namespace Larafast\Themes\View\Components;

use Illuminate\Support\Facades\View;
use Illuminate\View\Component;

class ElementComponent extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(String $name, Array $props = [])
    {
        $this->name = $name;
        $this->props = $props;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        if(View::exists($this->name)){
            return view($this->name)->with(
                [
                    'props' => $this->props,
                ]
            );
        }else{
            return 'Elemento não encontrado';
        }
    }
}
