<?php

return [
    'themes' => [
        'admin_minimal' => [
            'themeSchema' => \App\Larafast\ThemeSchemas\AdminMinimalThemeSchema::class
        ],
        'example_site' => [
            'themeSchema' => \App\Larafast\ThemeSchemas\SiteExampleThemeSchema::class
        ],
    ]
];
