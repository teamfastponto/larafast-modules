<a href="{{ route($params['routeNameEdit'], ['id' => $itemTable->{$config['keyAttribute'] }]) }}" class="btn btn-xs btn-primary">
    <i class="glyphicon glyphicon-edit"></i> {{ $params['label-btnEdit'] }}
</a>