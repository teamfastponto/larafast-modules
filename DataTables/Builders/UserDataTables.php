<?php

namespace Larafast\DataTables\Builders;

use Larafast\DataTables\Services\DataTablesBuilder;
use Larafast\Users\Entities\User;

class UserDataTables extends DataTablesBuilder{

    public function build(){

        return $this
        ->setModel(User::class);

        // ->addMenu('datatables::menu.user-menu',
        //     [//params
        //         'routeNameEdit' => 'edit.table',
        //         'label-btnEdit' => 'Editar'
        //     ],
        //     [//config
        //         'keyAttribute' => 'id'
        //     ]
        // );
        
        // ->join(TestRelation::class, [
        //     'type' => 'full',
        //     'originRelation' => 'id',
        //     'joinRelation' => 'idUser',
        //     'fields' => [
        //         [
        //             'name' => 'value',
        //             'as' => 'value_teste',
        //             'friendly_name' => 'Valor Relation'
        //         ]
        //     ]
        // ]);
    }
}
