<?php

namespace Larafast\DataTables\Http\Controllers;

use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class DataTablesController extends Controller{
    
    public function getData(Request $request){

        $model = decrypt($request->model);
        $columns = $request->columns;
        $joins = decrypt($request->joins);
        $joins = json_decode($joins, true);

        if(empty($model)){
            return DataTables::collection(collect([]))
            ->make(true); 
        }

        $query = $model::when(!empty($joins), function ($q) use($model, $joins){
            return $this->addJoins($model, $q, $joins);
        })
        ->selectRaw($this->getSelects($columns, $joins));

        $dataTable = DataTables::collection($query->get());

        if(isset($request->menu) && !empty($request->menu)){
            $menu = $request->menu;

            $dataTable->addColumn('menu', function ($itemTable) use ($menu) {
                $menu['itemTable'] = $itemTable;
                return view($menu['viewPath'])->with($menu);
            });
        }

        return $dataTable->make(true);
    }

    function addJoins($model, $q, $joins){
        $model = new $model();
        $originTable = $model->getTable();
        $operator = '=';

        foreach($joins as $join){
            
            $originRelation = $originTable . '.' . $join['originRelation'];
            $joinRelation = $join['table'] . '.' . $join['joinRelation'];

            switch ($join['type']) {
                case 'full':
                    $q->join($join['table'], $originRelation, $operator, $joinRelation);
                    break;
            }
        }

        return $q;
    }

    function getSelects($columns, $joins){
        $selects = [];
        $columnsDiff = [];
        $columns = array_column($columns, 'data');

        foreach($joins as $join){
            foreach(($join['fields'] ?? []) as $field){
                if(isset($field['name']) && isset($field['as'])){
                    array_push($columnsDiff, $field['as']);
                    array_push($selects, $field['name'] . ' as ' . $field['as']);
                }
            }
        }

        array_push($columnsDiff, 'menu');
        array_push($selects, 'id');

        $columns = array_diff($columns, $columnsDiff);
        $selects = array_merge($columns, $selects);

        return implode(", ", $selects);
    }
}