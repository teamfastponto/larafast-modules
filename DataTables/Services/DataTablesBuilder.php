<?php

namespace Larafast\DataTables\Services;

use Illuminate\Support\Facades\Log;

class DataTablesBuilder{
    protected $viewBase;
    protected $model = [];
    protected $joins = [];
    protected $fields = [];
    protected $menu = [];
    protected $route;

    function __construct()
    {
        $this->viewBase = 'datatables::base';
        $this->route = route('datatables.getData');
    }

    public function setModel(String $model){
        $this->model = $model;

        $fields = $this->getFields();
        $this->fields = array_merge($this->fields, $fields);

        return $this;
    }

    public function setView(String $viewPath){
        $this->viewBase = $viewPath;
        return $this;
    }

    public function join(String $model, Array $config){
        $model = new $model();
        $table = $model->getTable();

        array_push($this->joins, array_merge(['table' => $table], $config));

        $this->addJoinFields($table, $config['fields']);

        return $this;
    }
    
    public function addMenu(string $viewPath, array $params = [], array $config = []){

        $this->menu = [
            'viewPath' => $viewPath,
            'params' => $params,
            'config' => $config
        ];

        if(!isset($this->menu['config']['keyAttribute'])){
            $this->menu['config']['keyAttribute'] == 'id';
        }

        array_push($this->fields, [
            'data' => 'menu',
            'name' => 'menu',
            'friendly_name' => 'Menu'
        ]);
        
        return $this;
    }

    function addJoinFields($table, $fields){
        foreach($fields as $field){
            array_push($this->fields, [
                'data' => $field['as'],
                'name' => $table . '.' . $field['name'],
                'friendly_name' => $field['friendly_name']
            ]);
        }
    }

    public function setRoute($route){
        $this->route = $route;
        return $this;
    }

    public function getFields(){
        if(class_exists($this->model)){
            return $this->model::$dataTablesFields;
        }else{
            return [];
        }
    }

    public function render(){

        $html = view($this->viewBase)->with([
            'idTable' => 'table-' . md5(uniqid(Rand(), true)),
            'model' => $this->model,
            'joins' => $this->joins,
            'route' => $this->route,
            'fields' => $this->fields,
            'menu' => $this->menu
        ])->render();

        return $html;
    }
}