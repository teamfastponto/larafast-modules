<?php

namespace Larafast\Site\Services;

use Exception;
use Illuminate\Support\Facades\Log;
use Larafast\Site\Entities\SiteData;

class SiteService{

    public static function typeData(string $theme, string $element, string $ref){
        $themeSchame = config('themes.themes.' . $theme . '.themeSchema');

        //caso não existir o tema
        if(is_null($themeSchame)){}

        $typeElement = $themeSchame::getTypeDataElement($element, $ref);

        if(!isset($typeElement['type'])){
            throw new Exception('Array type with not data defined', 1);
        }

        return [
            'type' => $typeElement['type'],
            'keys' => $typeElement['keys'] ?? []
        ];
    }

    public static function getData(string $theme, string $element, array $options = []){

        if (!session()->has('language')) {
            session(['language' => 'pt-BR']);
        }

        $language = session()->get('language');

        $data = SiteData::where('themeName', $theme)
        ->where('elementName', $element)
        ->where('language', $language)
        ->when(isset($options['ref']), function($q) use($options){
            $q->where('ref', $options['ref']);
        })
        ->when(isset($options['idElement']), function($q) use($options){
            $q->where('idElement', $options['idElement']);
        })
        ->orderBy('id', 'DESC')
        ->get();

        return $data;
    }

    public static function getDataElement($schemaElement){

        $schemaElement = array_merge([
            'theme' => null,
            'element' => null,
            'options' => []
        ], $schemaElement);

        extract($schemaElement, EXTR_OVERWRITE);

        $elementsData = SiteService::getData($theme, $element, $options);

        $arrayElementsData = [];
        foreach($elementsData as $elementData){
            $data = json_decode($elementData->data, true);
            $arrayElementsData = array_merge($arrayElementsData, [$elementData->ref => $data[$elementData->ref]]);
        }

        return $arrayElementsData;
    }
}