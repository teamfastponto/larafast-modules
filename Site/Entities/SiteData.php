<?php

namespace Larafast\Site\Entities;

use Illuminate\Database\Eloquent\Model;

class SiteData extends Model
{
    protected $table = 'lf_site_data';

    protected $fillable = [
        'themeName',
        'elementName',
        'idElement',
        'ref',
        'language',
        'data'
    ];
}
