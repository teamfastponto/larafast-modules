<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::prefix('site')->group(function() {
    Route::post('/element/get-content', 'SiteController@getContentElement')->name('site.element.getContent');
    Route::post('/element/post-content', 'SiteController@postContentElement')->name('site.element.postContent');
    Route::get('/set-language/{codeLanguage}', 'SiteController@setLanguage')->name('site.setLanguage');
});
