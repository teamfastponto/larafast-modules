<?php

namespace Larafast\Site\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use Larafast\Site\Services\SiteService;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Larafast\Site\Entities\SiteData;

class SiteController extends Controller{

    use FormBuilderTrait;

    function getContentElement(Request $request){
        $theme = $request->theme;
        $element = $request->element;
        $ref = $request->ref;
        $options = $request->options;
        $options = array_merge($options, ['ref' => $ref]);

        $type = SiteService::typeData($theme, $element, $ref);

        $schema = [
            'theme' => $theme,
            'element' => $element,
            'ref' => $ref,
            'options' => $options
        ];

        $dataElement = SiteService::getDataElement($schema);
        $dataElement = [
            'data' => $dataElement,
            'schema' => $schema
        ];

        try{    
            $form = $this->buildFormConteudo($type, $dataElement);
            $form = form($form);

            return response()->json([
                'success' => true,
                'form' => $form
            ]);
        }catch(Exception $e){
            Log::error($e);

            return response()->json([
                'success' => false,
                'error' => $e
            ], 400);
        }
    }

    function postContentElement(Request $request){
        $themeName = decrypt($request->lf_schema_theme);
        $elementName = decrypt($request->lf_schema_element);
        $ref = decrypt($request->lf_schema_ref);
        $options = json_decode(decrypt($request->lf_schema_options), true);
        $options = array_merge($options, [
            'ref' => $ref,
        ]);

        $fields = $request->except([
            '_token',
            'language',
            'lf_schema_theme',
            'lf_schema_element',
            'lf_schema_ref',
            'lf_schema_options'
        ]);

        $dataElement = SiteService::getData($themeName, $elementName, $options);

        $saveData = ($dataElement->count() > 0) ? $dataElement->first() : new SiteData();
        $saveData->themeName = $themeName;
        $saveData->elementName = $elementName;
        $saveData->idElement = isset($options['idElement']) ? $options['idElement'] : null;
        $saveData->ref = $ref;
        $saveData->language = session()->get('language');
        $saveData->data = json_encode($fields);
        $saveData->save();

        return response()->json($saveData);
    }

    function buildFormConteudo(array $type, array $dataElement){
        $formOptions = [
            'id' => 'lf-form-site-content',
            'url' => '#',
            'method' => 'POST'
        ];

        $form = $this->plain($formOptions);

        if($type['type'] === 'array'){

            foreach($type['keys'] as $arrayKey){

                $nameExplode = explode('.', $arrayKey['name']);

                $inputName = $dataElement['schema']['ref'];
                foreach($nameExplode as $key => $itemName){
                    $inputName = $inputName . '[' . $itemName . ']';
                }

                $form->add($inputName, $arrayKey['type'], [
                    'label' => $arrayKey['name'],
                    'attr' => ['placeholder' => $arrayKey['name'] . '_' . $dataElement['schema']['ref']]
                ]);
            }

        }else{
            $inputName = $dataElement['schema']['element'] . '_' . $dataElement['schema']['ref'];
            $form->add($dataElement['schema']['ref'], $type['type'], [
                'label' => $inputName,
                'attr' => ['placeholder' => $inputName],
                'value' => $dataElement['data'][$dataElement['schema']['ref']] ?? null
            ]);
        }

        foreach($dataElement['schema'] as $key => $schema){
            $form->add('lf_schema_' . $key, 'hidden', [
                'value' => encrypt((is_array($schema) ? json_encode($schema) : $schema))
            ]);
        }

        return $form;
    }

    function setLanguage($codeLanguage){
        session(['language' => $codeLanguage]);
        return redirect()->back();
    }
}
