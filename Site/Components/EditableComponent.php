<?php

namespace Larafast\Site\Components;

use Illuminate\Support\Facades\View;
use Illuminate\View\Component;

class EditableComponent extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $theme, string $element, array $options = [])
    {
        $this->theme = $theme;
        $this->element = $element;
        $this->options = $options;
    }

    public function getSchemaElement(){
        return setSchemaElement($this->theme, $this->element, $this->options);
    }

    public function get($key){
        return getDataElement($this->getSchemaElement())[$key] ?? $key;
    }

    public function ref(string $ref)
    {
        $elementData = $this->getSchemaElement();
        $elementData['ref'] = $ref;
        return json_encode($elementData);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return <<<'blade'
            {{ $slot }}
        blade;
    }
}
