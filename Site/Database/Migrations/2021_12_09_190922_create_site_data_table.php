<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiteDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lf_site_data', function (Blueprint $table) {
            $table->id();
            $table->string('themeName', 100);
            $table->string('elementName', 150);
            $table->string('idElement', 100)->nullable();
            $table->string('ref', 100);
            $table->string('language', 50);
            $table->text('data');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lf_site_data');
    }
}
