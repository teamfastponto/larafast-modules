<?php

use Illuminate\Support\Facades\Log;
use Illuminate\View\Component;
use Larafast\Site\Services\SiteService;

if (! function_exists('setSchemaElement')) {
    function setSchemaElement(string $theme, string $element, array $options)
    {
        return [
            'theme' => $theme,
            'element' => $element,
            'options' => $options
        ];
    }
}


if (! function_exists('editableRef')) {
    function editableRef($component, string $ref)
    {
        $elementData = $component->getSchemaElement();
        $elementData['ref'] = $ref;
        return json_encode($elementData);
    }
}

if (! function_exists('siteRef')) {
    function siteRef($component, string $ref)
    {
        return editableRef($component, $ref);
    }
}

if (! function_exists('getDataElement')) {
    function getDataElement(array $schemaElement)
    {
        return SiteService::getDataElement($schemaElement);
    }
}