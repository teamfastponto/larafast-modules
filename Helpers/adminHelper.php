<?php

use Larafast\Admin\Facades\DashboardMenuFacade;

if (! function_exists('adminElement')) {
    function adminElement($element)
    {
        $adminTheme = config('admin.theme_default');

        if(!empty($adminTheme)){
            return 'themes::' . $adminTheme . '.elements.' .  $element;
        }else{
            return null;
        }
    }
}

if (!function_exists('dashboard_menu')) {
    /**
     * @return \Botble\Base\Supports\DashboardMenu
     */
    function dashboard_menu()
    {
        return DashboardMenuFacade::getFacadeRoot();
    }
}

if (! function_exists('mainMenu')) {
    function mainMenu()
    {
        return collect(dashboard_menu()->getMenu('mainMenu'));
    }
}

if (! function_exists('topMenu')) {
    function topMenu()
    {
        return collect(dashboard_menu()->getMenu('topMenu'));
    }
}

if (! function_exists('userMenu')) {
    function userMenu()
    {
        return collect(dashboard_menu()->getMenu('userMenu'));
    }
}

if (! function_exists('adminTheme')) {
    function adminTheme()
    {
        return config('admin.theme_default');
    }
}

if (! function_exists('adminView')) {
    function adminView($path)
    {
        return 'themes::' . adminTheme() . '.views.' . $path;
    }
}

if (! function_exists('isAdminColorTheme')) {
    function isAdminColorTheme($color)
    {
        if(!session()->has('colorTheme') && $color == 'light'){
            return true;
        }elseif(session('colorTheme') == $color){
            return true;
        }else{
            return false;
        }
    }
}

if (! function_exists('adminColorTheme')) {
    function adminColorTheme()
    {
        if(!session()->has('colorTheme')){
            return 'light';
        }else{
            return session('colorTheme');
        }
    }
}