<?php

if (! function_exists('viewTheme')) {
    function viewTheme($theme, $path)
    {
        return 'themes::' . $theme . '.'  . $path;
    }
}

if (! function_exists('themeElement')) {
    function themeElement($theme, $path)
    {
        return 'themes::' . $theme . '.elements.'  . $path;
    }
}

if (! function_exists('theme_asset')) {
    function theme_asset($theme, $path = null)
    {
        return asset('themes/' . $theme . $path);
    }
}

if (! function_exists('headerTheme')) {
    function headerTheme($theme)
    {
        return 'themes::' . $theme . '.layouts.header';
    }
}

if (! function_exists('scriptsTheme')) {
    function scriptsTheme($theme)
    {
        return 'themes::' . $theme . '.layouts.scripts';
    }
}