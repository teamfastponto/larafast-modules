<?php

namespace Larafast\Users\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class UserRoleEnum extends Enum
{
    const ADMIN = 'admin';
    const USER = 'user';
}
