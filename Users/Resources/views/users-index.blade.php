@extends('themes::' . adminTheme() . '.layouts.master')
@section('titlePage')
Usuários
@endsection
@section('content')
    <x-element :name="adminElement('header-page')" :props="[
        'title_header' => 'Usuários',
        'breadcrumb' => [
            [
                'name' => 'Usuários',
                'url' => '#'
            ]
        ]
    ]"/>

    <x-element :name="adminElement('card-full')" :props="[
        'titleCard' => 'Listagem'
    ]">
        <x-slot name="contentCard">
            @datatable(['dataTable' => $dataTable])
        </x-slot>
    </x-element>
@endsection