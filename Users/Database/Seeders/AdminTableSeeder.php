<?php

namespace Larafast\Users\Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Larafast\Users\Enums\UserRoleEnum;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $adminEmail = 'admin@larafast.com';
        $admin = User::where('email', $adminEmail)->first();

        if(!isset($admin->id)){
            User::insert([
                'name' => 'admin',
                'email' => $adminEmail,
                'role' => UserRoleEnum::ADMIN,
                'password' => Hash::make('admin123'),
            ]);
        }
    }
}
