<?php

namespace Larafast\Users\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'role',
        'password',
    ];

    public static $dataTablesFields = [
        [
            'data' => 'name',
            'name' => 'name',
            'friendly_name' => 'Nome'
        ],
        [
            'data' => 'email',
            'name' => 'email',
            'friendly_name' => 'E-mail'
        ],
        [
            'data' => 'role',
            'name' => 'role',
            'friendly_name' => 'Função'
        ]
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
