<?php

namespace Larafast\Admin\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class AdminServiceProvider extends ServiceProvider
{
    /**
     * @var string $moduleName
     */
    protected $moduleName = 'Admin';

    /**
     * @var string $moduleNameLower
     */
    protected $moduleNameLower = 'admin';

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerViews();
        $this->registerPublishs();
        $this->loadMigrationsFrom(__DIR__ . '../Database/Migrations');
    }

    public function registerPublishs(){
        $this->publishes([
            dirname(__DIR__, 1) . "/publishable/providers" => app_path('Larafast/Providers'),
        ], ['admin-provider']);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/' . $this->moduleNameLower);

        $sourcePath = dirname(__DIR__, 1) . '/Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ], ['views', $this->moduleNameLower . '-module-views']);

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);

        Blade::include('admin::layouts.theme_extends_default', 'themeExtends');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        // $langPath = resource_path('lang/modules/' . $this->moduleNameLower);

        // if (is_dir($langPath)) {
        //     $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
        // } else {
        //     $this->loadTranslationsFrom(dirname(__DIR__, 1) . '/Resources/lang', $this->moduleNameLower);
        // }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (\Config::get('view.paths') as $path) {
            if (is_dir($path . '/modules/' . $this->moduleNameLower)) {
                $paths[] = $path . '/modules/' . $this->moduleNameLower;
            }
        }
        return $paths;
    }
}
