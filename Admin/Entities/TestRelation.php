<?php

namespace Larafast\Admin\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TestRelation extends Model
{
    use HasFactory;

    protected $table = 'test_relations';
    
    protected $fillable = [];
}
