<?php

namespace Larafast\Admin\Facades;

use Illuminate\Support\Facades\Facade;
use Larafast\Admin\Menu\DashboardMenu;

class DashboardMenuFacade extends Facade{

    protected static function getFacadeAccessor()
    {
        return DashboardMenu::class;
    }
}

