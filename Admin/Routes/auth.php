<?php

use Illuminate\Support\Facades\Route;

Route::get('login', 'LoginController@showLogin')->name('login');
Route::post('login', 'LoginController@login')->name('admin.login.post');

Route::post('logout', 'LoginController@logout')->name('logout');