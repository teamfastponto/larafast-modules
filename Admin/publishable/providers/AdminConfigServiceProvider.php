<?php

namespace App\Larafast\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Events\RouteMatched;
use Illuminate\Support\Facades\Event;

class AdminConfigServiceProvider extends ServiceProvider{
    public function boot()
    {
        $this->registerMenus();
        $this->setConfig();
    }

    public function setConfig(){
        app()->config["admin"] = [
            'theme_default' => 'admin_minimal',
            'logo_url' => theme_asset('admin_minimal', '/img/logos/larafast_logo.png'),
            'name_panel' => 'Larafast',
            'login_route' => 'admin.showLogin',
            'topbar' => [
                'search' => true,
                'label_search' => 'Pesquisar...',
                'user_menu' => [
                    'name' => 'User',
                    'url_image' => theme_asset('admin_minimal', '/img/avatar.jpg'),
                ]
            ]
        ];
    }

    public function registerMenus(){
        Event::listen(RouteMatched::class, function(){
            dashboard_menu()->registerItem('mainMenu', [
                'session' => strtoupper('configurações'),
                'name' => 'users',
                'friendly_name' => 'Usuários',
                'icon' => 'fas fa-fw fa-user',
                'url' => '#',
                'permissions' => 'users.index',
                'hasChildren' => true,
                'childrens' => [
                    [
                        'name' => 'index',
                        'friendly_name' => 'Listar',
                        'url' => route('users.index')
                    ]
                ]
            ]);
        });
    }
}