<?php

namespace Larafast\Admin\Menu;

use Illuminate\Support\Facades\Log;

class DashboardMenu{
    protected $mainMenu;
    protected $topMenu;
    protected $userMenu;

    function __construct()
    {
        $this->mainMenu = [];
        $this->topMenu = [];
        $this->userMenu = [];
    }

    public function registerItem(String $type, Array $menu){
        switch ($type) {
            case 'mainMenu':
                $this->registerMainMenu($menu);
            break;
            case 'topMenu':
                $this->registerTopMenu($menu);
            break;
            case 'userMenu':
                $this->registerUserMenu($menu);
            break;
        }

        return $this;
    }

    function registerMainMenu($menu){
        array_push($this->mainMenu, $menu);
    }

    function registerTopMenu($menu){
        array_push($this->topMenu, $menu);
    }

    function registerUserMenu($menu){
        array_push($this->userMenu, $menu);
    }

    function getMenu(String $type){
        $menu = [];

        switch ($type) {
            case 'mainMenu':
                $menu = $this->mainMenu;
            break;
            case 'topMenu':
                $menu = $this->topMenu;
            break;
            case 'userMenu':
                $menu = $this->userMenu;
            break;
        }

        return $menu;
    }
}