<?php

namespace Larafast\Larafast\Command;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class LarafastInstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larafast:install
    {--force : reinstall components}
    {--migrate-fresh : reset database}
    {--noseed : reset database}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cria estrutura para um novo tema';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $force = $this->option('force');
        $migrateFresh = $this->option('migrate-fresh');
        $noSeed = $this->option('noseed');

        $commands = [
            [
                'type' => 'publish',
                'command' => 'vendor:publish',
                'attr' => [
                    '--tag' => 'larafast-welcome',
                    '--force' => true
                ]
            ],
            [
                'type' => 'publish',
                'command' => 'vendor:publish',
                'attr' => [
                    '--tag' => 'themes',
                    '--force' => $force
                ]
            ],
            [
                'type' => 'publish',
                'command' => 'vendor:publish',
                'attr' => [
                    '--tag' => 'admin',
                    '--force' => $force
                ]
            ],
            [
                'type' => 'publish',
                'command' => 'vendor:publish',
                'attr' => [
                    '--tag' => 'admin-provider',
                    '--force' => $force
                ]
            ],
            [
                'type' => 'migrate',
                'command' => 'migrate' . ($migrateFresh ? ':fresh' : null),
            ],
            [
                'type' => 'seed',
                'command' => 'db:seed',
                'attr' => [
                    '--class' => 'Larafast\\Users\\Database\\Seeders\\UsersDatabaseSeeder',
                ]
            ],
        ];

        $progressBar = $this->output->createProgressBar(count($commands));

        $progressBar->start();

        foreach ($commands as $command) {

            if(isset($command['command'])){
                if(isset($command['type']) && $command['type'] == 'seed' && !$noSeed){
                    Artisan::call($command['command'], $command['attr'] ?? []);
                }else{
                    Artisan::call($command['command'], $command['attr'] ?? []);
                }
            }

            $progressBar->advance();
        }

        $progressBar->finish();

        $this->info('Larafast Instalado com sucesso!');
    }
}
