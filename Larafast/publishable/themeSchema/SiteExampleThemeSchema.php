<?php

namespace App\Larafast\ThemeSchemas;

use Larafast\Themes\Support\BaseThemeSchema;

class SiteExampleThemeSchema extends BaseThemeSchema
{

    protected static $name = 'example_site';

    protected static $prefix = 'site';

    // Array map elements
    // [
    //     'name' => 'element_name',
    //     'view' => 'view_name',
    //     'items' => [
    //         [
    //             'name' => 'name_item',
    //             'type' => 'text|formated_text|list|number|file|multipleFile|url|array|html'
    //         ],
    //         [
    //             'name' => 'menu',
    //             'type' => 'array',
    //             'keys' => [
    //                 [
    //                     'name' => 'name_key',
    //                     'type' => 'text|formated_text|list|number|file|multipleFile|url|array|html'
    //                 ]
    //             ]
    //         ]
    //     ]
    // ]

    protected static $elements = [
        [
            'name' => 'page',
            'view' => 'page'
        ],
        [
            'name' => 'top-nav',
            'view' => 'top-nav',
            'items' => [
                [
                    'name' => 'titleTopNav',
                    'type' => 'text'
                ],
                [
                    'name' => 'menu',
                    'type' => 'array',
                    'keys' => [
                        'name',
                        'friendly_name',
                        'url',
                        'dropdown_items.name',
                        'dropdown_items.friendly_name',
                        'dropdown_items.url',
                    ]
                ]
            ]
        ],
        [
            'name' => 'header-section',
            'view' => 'header-section',
            'items' => [
                [
                    'name' => 'img',
                    'type' => 'file'
                ],
                [
                    'name' => 'title',
                    'type' => 'text'
                ],
                [
                    'name' => 'buttons',
                    'type' => 'array',
                    'keys' => [
                        [
                            'name' => 'mainButton.text',
                            'type' => 'text'
                        ],
                        [
                            'name' => 'mainButton.url',
                            'type' => 'url'
                        ],
                        [
                            'name' => 'secondButton.text',
                            'type' => 'text'
                        ],
                        [
                            'name' => 'secondButton.url',
                            'type' => 'url'
                        ],
                    ]
                ],
            ]
        ],[
            'name' => 'features-section',
            'view' => 'features-section'
        ]

    ];

}