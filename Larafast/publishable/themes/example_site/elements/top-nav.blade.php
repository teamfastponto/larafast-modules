<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container px-5">
        <a class="navbar-brand" href="#">{{ $titleTopNav ?? 'titleTopNav' }}</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                @foreach ($menu ?? [] as $itemMenu)
                    {{ $slotMenu ?? null }}
                    @if(isset($itemMenu['dropdown_items']))
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" id="navbarDropdown-{{ $itemMenu['name'] }}" role="button" data-bs-toggle="dropdown" aria-expanded="false">{!! $itemMenu['friendly_name'] !!}</a>
                            <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown-{{ $itemMenu['name'] }}">
                                @foreach (($itemMenu['dropdown_items'] ?? []) as $dropdownItems)
                                    <li><a class="dropdown-item" href="{{ $dropdownItems['url'] ?? '#' }}">{!! $dropdownItems['friendly_name'] !!}</a></li>
                                @endforeach
                            </ul>
                        </li>
                    @else
                        <li class="nav-item"><a class="nav-link" href="{{ $itemMenu['url'] ?? '#' }}">{!! $itemMenu['friendly_name'] !!}</a></li>
                    @endif
                @endforeach
            </ul>
        </div>
    </div>
</nav>