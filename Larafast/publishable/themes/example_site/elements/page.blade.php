@extends(viewTheme('example_site', 'layouts.master'))
@section('content')
    {{ $slot }}
@endsection
@section('footer')
    @include(themeElement('example_site', 'footer'))
@endsection