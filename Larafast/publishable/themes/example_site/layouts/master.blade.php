<!DOCTYPE html>
@php
    $theme = 'example_site'
@endphp
<html lang="pt_br">
    @include(headerTheme('example_site'))
    <body class="d-flex flex-column h-100">
        <main class="flex-shrink-0">
            @yield('content')
        </main>
        @yield('footer')
        @include(scriptsTheme('example_site'))
        @larafastIncludes
    </body>
</html>