<x-site-page>
    <x-site-top-nav
    titleTopNav="Larafast"
    :menu="[
        [
            'name' => 'menu',
            'friendly_name' => 'Menu1',
            'url' => 'https://google.com.br'
        ],
        [
            'name' => 'menu',
            'friendly_name' => 'Menu2',
            'dropdown_items' => [
                [
                    'name' => 'menu',
                    'friendly_name' => 'Item1',
                    'url' => 'https://google.com.br'
                ]
            ]
        ],
        [
            'name' => 'language',
            'friendly_name' => 'Language',
            'dropdown_items' => [
                [
                    'name' => 'pt-BR',
                    'friendly_name' => 'Português Brasil',
                    'url' => route('site.setLanguage', ['codeLanguage' => 'pt-BR'])
                ],
                [
                    'name' => 'pt',
                    'friendly_name' => 'Português',
                    'url' => route('site.setLanguage', ['codeLanguage' => 'pt'])
                ],
                [
                    'name' => 'en',
                    'friendly_name' => 'English',
                    'url' => route('site.setLanguage', ['codeLanguage' => 'en'])
                ],
                [
                    'name' => 'es',
                    'friendly_name' => 'Espanõl',
                    'url' => route('site.setLanguage', ['codeLanguage' => 'es'])
                ]
            ]
        ]
    ]"
    />

    <x-site-header-section
        idElement="header-section-home"
    />
    <x-site-features-section />
</x-site-page>