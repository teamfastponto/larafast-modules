$(function(){
    $.contextMenu({
        selector: '.lf-site-edit', 
        trigger: 'hover',
        delay: 500,
        autoHide: true,
        callback: function(key, options) {
            if(key === 'edit'){
                clickContextMenu(key, options);
            }
        },
        items: {
            "edit": {name: "Editar", icon: "edit"},
            "quit": {name: "Fechar", icon: function(){ return 'context-menu-icon context-menu-icon-quit'; }}
        }
    });
});

function clickContextMenu(key, options){

    const elementModal = document.getElementById('lf-edit-modal');
    const editModal = new bootstrap.Modal(elementModal, {
        keyboard: false
    });

    elementModal.addEventListener('hidden.bs.modal', function (event) {
        $("#site-content-loader").show();
        $("#site-content-edit").empty();
    })

    const elementRef = JSON.parse(options.$trigger.attr('element-ref'));
    sendAjaxContextMenu(elementRef);

    editModal.show();
}

function sendAjaxContextMenu(themeRef){
    const route = $('#route-elementGetContent').val();

    axios.post(route, themeRef, {
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })
    .then(function (response) {
        if(response.data.success){
            $("#site-content-loader").hide();
            $("#site-content-edit").html(response.data.form);
        }
    })
    .catch(function (error) {
        console.log(error);
    });
}

$('.lf-btn-save-content').click(function(){
    postElementContent();
});

function postElementContent(){
    const formData = $("#lf-form-site-content").serialize();
    const route = $('#route-elementPostContent').val();

    axios.post(route, formData, {
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })
    .then(function (response) {
        if(response.status == 200){
            window.location = window.location.pathname;
        }
    })
    .catch(function (error) {
        console.log(error);
    });
}