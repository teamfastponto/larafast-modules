<?php

namespace Larafast\Larafast\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Larafast\Admin\Providers\AdminServiceProvider;
use Larafast\DataTables\Providers\DataTablesServiceProvider;
use Larafast\Forms\Providers\FormsServiceProvider;
use Larafast\Site\Providers\SiteServiceProvider;
use Larafast\Larafast\Command\LarafastInstallCommand;
use Larafast\Larafast\Support\FindProviders;
use Larafast\Themes\Providers\ThemesServiceProvider;
use Larafast\Users\Providers\UsersServiceProvider;

class LarafastServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->registerViews();
        $this->registerPublishs();

        $this->commands([
            LarafastInstallCommand::class
        ]);

        Blade::include('larafast::partial.react', 'react');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerProviders();
        $this->registerHelpers();
    }

    public function registerProviders(){
        $providers = [
            AdminServiceProvider::class,
            DataTablesServiceProvider::class,
            FormsServiceProvider::class,
            SiteServiceProvider::class,
            ThemesServiceProvider::class,
            UsersServiceProvider::class
        ];

        $providersPublished = FindProviders::getProviders('App\\Larafast\\Providers', app_path('Larafast/Providers'));

        $providers = array_merge($providers, $providersPublished);

        foreach($providers as $provider){
            $this->app->register($provider);
        }
    }

    public function registerPublishs(){
        $this->publishes([
            dirname(__DIR__, 1) . "/publishable/public" => base_path('public'),
            dirname(__DIR__, 1) . '/publishable/themes' => base_path('themes'),
            dirname(__DIR__, 1) . '/publishable/themeSchema' => app_path('Larafast/ThemeSchemas'),
            dirname(__DIR__, 1) . '/publishable/resources/views' => resource_path('views'),
        ], ['larafast-welcome']);
    }

    public function registerViews()
    {
        $this->loadViewsFrom(dirname(__DIR__, 1) . '/Views', 'larafast');
        Blade::include('larafast::partial.includes', 'larafastIncludes');
    }

    public function registerHelpers(){
        foreach (glob(dirname(__DIR__, 2) . '/Helpers/*.php') as $helpersfilename){
            require_once($helpersfilename);
        }
    }
}
