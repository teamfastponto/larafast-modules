<?php

namespace Larafast\Larafast\Support;

use Illuminate\Support\Facades\File;

class FindProviders {

	public static function getProviders($namespace, $path)
	{
		if(!File::exists($path)){
			return [];
		}
		
		$files = File::files($path);
		$providers = [];

		foreach($files as $file){

			$fileInfo = pathinfo($file);
			$filename = $fileInfo['filename'];

			if (class_exists($namespace . '\\' . $filename)) {
				array_push($providers, $namespace . '\\' . $filename);
			}
		}

		return $providers;
	}


}