<style>
    .lf-editable:hover{
        border: 1px red dashed;
    }

    .loader {
        border: 16px solid #f3f3f3; /* Light grey */
        border-top: 16px solid #3498db; /* Blue */
        border-radius: 50%;
        width: 120px;
        height: 120px;
        animation: spin 2s linear infinite;
    }

    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
</style>

<link rel="stylesheet" href="{{ asset('larafast/vendor') }}/contextMenu/jquery.contextMenu.min.css">

<meta name="csrf-token" content="{{ csrf_token() }}">
<input type="hidden" id="route-elementGetContent" value="{{ route('site.element.getContent') }}">
<input type="hidden" id="route-elementPostContent" value="{{ route('site.element.postContent') }}">

<!-- Modal -->
<div class="modal fade" id="lf-edit-modal" tabindex="-1" aria-labelledby="lf-edit-modal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Editar conteúdo</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <h4>Language: {{ session()->get('language') }}</h4>
            <span id="site-content-edit"></span>
            <div id="site-content-loader" class="loader"></div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
            <button type="button" class="btn btn-primary lf-btn-save-content">Salvar</button>
        </div>
        </div>
    </div>
</div>

<script src="{{ asset('larafast/vendor') }}/contextMenu/jquery.contextMenu.min.js"></script>
<script src="{{ asset('larafast/vendor') }}/contextMenu/jquery.ui.position.js"></script>
<script src="{{ asset('larafast/vendor') }}/axios/axios.min.js"></script>
<script src="{{ asset('larafast') }}/larafast.js"></script>